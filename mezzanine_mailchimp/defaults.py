from mezzanine.conf import register_setting

register_setting(
    name="MAILCHIMP_LIST_ID",
    description="List Id de Mailchimp.",
    editable=True,
    default='',
)

register_setting(
    name="MAILCHIMP_API_KEY",
    description="Api Key de Mailchimp.",
    editable=True,
    default='',
)

register_setting(
    name="MAILCHIMPFORM_SUSCRIBER_ACTION_FROM",
    description="Suscriber action form.",
    editable=True,
    default='',
    label='Mailchimp action Form'
)

register_setting(
    name="MAILCHIMPFORM_SUSCRIBER_TITLE",
    label='Mailchimp Suscriber title',
    description="Suscriber title.",
    editable=True,
    default='',
)

register_setting(
    name="MAILCHIMPFORM_EMAIL_FIELD_NAME",
    label='Mailchimp Suscriber Email field name',
    description="Email field name.",
    editable=True,
    default='',
)

register_setting(
    name="MAILCHIMPFORM_SUBMIT_BUTTON",
    label='Mailchimp submit button text',
    description="Mailchimp submit button text",
    editable=True,
    default='',
)

register_setting(
    name="MAILCHIMPFORM_HIDDEN_FIELD_NAME",
    label='Mailchimp hidden field name',
    description="Mailchimp hidden field name",
    editable=True,
    default='',
)