from django import template
from django.template.loader import render_to_string

from mezzanine.conf import settings

register = template.Library()


@register.simple_tag
def get_mailchimp_suscriber_form():

    return render_to_string(
        'mezzanine_mailchimp/includes/suscriber_form.html',
        {
            'action_form': settings.MAILCHIMPFORM_SUSCRIBER_ACTION_FROM,
            'title': settings.MAILCHIMPFORM_SUSCRIBER_TITLE,
            'field_name': settings.MAILCHIMPFORM_EMAIL_FIELD_NAME,
            'submit_text': settings.MAILCHIMPFORM_SUBMIT_BUTTON,
            'hidden_field': settings.MAILCHIMPFORM_HIDDEN_FIELD_NAME,
        }
    )

